//
//  ViewController.swift
//  Helper
//
//  Created by Syncrhonous on 8/5/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import Foundation
import UIKit
import CoreMotion


var vSpinner : UIView?
class ViewController: UIViewController {

    @IBOutlet var uiView: UIView!


    override func viewDidLoad() {
        super.viewDidLoad()
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    
    @IBAction func goToSecondPage(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.alertActivityIndicatorDisplay()
        }
    }
    

    
    @IBAction func showPro(_ sender: UIButton) {
        
    }
    
   
    
    @IBAction func portraitView(_ sender: UIButton) {
        
    }
    @IBAction func landscapeView(_ sender: UIButton) {
        //setUserOrientationLa()
        //StopAccelerometerOrientation()
    }
    
    @IBAction func unlockOrientation(_ sender: Any) {
        findingScreenHeightWidth()
    }
    
    
    func findingScreenHeightWidth(){
        let screenSize: CGRect = UIScreen.main.bounds
        
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        print("Screen width = \(screenWidth), screen height = \(screenHeight)")
    }
    
    func alertActivityIndicatorDisplay(){
        self.showSpinner(onView: self.view, msg: "Loading...")

        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.removeSpinner()
            //while vSpinner == nil {
               // print("kkhgr")
           // }
            
        }
        self.performSegue(withIdentifier: "second", sender: nil)
        
    }
    
//
//    let motionManager = CMMotionManager()
//    @objc
//    static func requiresMainQueueSetup()-> Bool {
//        return true
//    }
//
//    @objc
//    func toggleOrientation()-> Void {
//        DispatchQueue.main.async{
//            if UIDevice.current.orientation.isPortrait{
//                let value = UIInterfaceOrientation.landscapeRight.rawValue
//                UIDevice.current.setValue(value,forKey:"orientation")
//            }else{
//                let value = UIInterfaceOrientation.portrait.rawValue
//                UIDevice.current.setValue(value,forKey:"orientation")
//            }
//        }
//    }
//
//    @objc
//    func setUserOrientationPo () -> Void {
//        DispatchQueue.main.async{
//            let value = UIInterfaceOrientation.portrait.rawValue
//            UIDevice.current.setValue(value,forKey:"orientation")
//        }
//    }
//
//    @objc
//    func setUserOrientationLa () -> Void {
//        DispatchQueue.main.async{
//            let value = UIInterfaceOrientation.landscapeRight.rawValue
//            UIDevice.current.setValue(value,forKey:"orientation")
//        }
//    }
//
//    @objc
//    func StartAccelerometerOrientation () -> Void {
//        if self.motionManager.isAccelerometerAvailable{
//            print("avail")
//
//            //self.motionManager.accelerometerUpdateInterval = 1.0 / 60.0
//            DispatchQueue.main.async{
//                let value = UIInterfaceOrientation.portrait.rawValue
//                UIDevice.current.setValue(value,forKey:"orientation")
//
////                self.motionManager.startAccelerometerUpdates(to: OperationQueue.current!, withHandler: {(data : CMAccelerometerData?, error : Error?) in
////                    if (data != nil) {
////                        if let x = data?.acceleration.x {
////                            if x >= 0.65 {
////                                DispatchQueue.main.async{
////                                    let value = UIInterfaceOrientation.landscapeLeft.rawValue
////                                    UIDevice.current.setValue(value,forKey:"orientation")
////                                }
////                            }
////                            if x <= -0.65 {
////                                DispatchQueue.main.async{
////                                    let value = UIInterfaceOrientation.landscapeRight.rawValue
////                                    UIDevice.current.setValue(value,forKey:"orientation")
////                                }
////                            }
////                        }
////                    }
////                }
////                )
//            }
//        }
//    }
//
//    @objc
//    func StopAccelerometerOrientation () -> Void {
//        self.motionManager.stopAccelerometerUpdates()
//    }


}

var backGroundView : UIView?
var whiteView : UIView?
var loadingLabel : UILabel?

extension UIViewController {
    func showSpinner(onView : UIView, msg :String) {
        
        backGroundView = UIView(frame: CGRect(x: 0, y: 0, width: onView.frame.size.width, height: onView.frame.size.height))
        backGroundView!.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.view.addSubview(backGroundView!)
        
        whiteView = UIView(frame: CGRect(x: backGroundView!.frame.size.width / 2 - 75, y: backGroundView!.frame.size.height / 2 - 50, width: 150, height: 100))
        whiteView!.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        whiteView?.layer.cornerRadius = 8
        backGroundView?.addSubview(whiteView!)
        
        
        loadingLabel = UILabel(frame: CGRect(x: 0 , y: whiteView!.frame.size.width/2, width: whiteView!.frame.size.width, height: 20))
        //To set the color
        //loadingLabel!.backgroundColor = UIColor.white
        loadingLabel!.textColor = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        //To set the font Dynamic
        //loadingLabel!.font = UIFont(name: "Helvetica-Regular", size: 20.0)
        //To set the system font
        //loadingLabel!.font = UIFont.systemFont(ofSize: 20.0)
        loadingLabel!.textAlignment = .center
        loadingLabel!.text = msg
        whiteView!.addSubview(loadingLabel!)
        

        let spinnerView = UIView.init(frame: whiteView!.bounds)
        //spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.6)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.color = #colorLiteral(red: 0.6799238037, green: 0.3529885309, blue: 0.7658930421, alpha: 1)
        ai.startAnimating()
        ai.center = spinnerView.center

        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            whiteView!.addSubview(spinnerView)
        }

        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            whiteView?.removeFromSuperview()
            backGroundView?.removeFromSuperview()
            vSpinner = nil
        }
    }
}

